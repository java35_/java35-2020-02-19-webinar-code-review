package telran.printer;

public class PrinterAppl {

	private static final int N_PRINTERS = 4;
	
	int number = 10;
	
	public void foo1() {
		System.out.println();
	}
	
	public static void main(String[] args){
		Printer[] printers = new Printer[N_PRINTERS];
	
		printers[printers.length - 1] = new Printer(printers.length);
		for (int i = printers.length - 2; i >= 0; i--) {
			printers[i] = new Printer(i + 1, printers[i + 1]);
		}
		printers[printers.length - 1].setReference(printers[0]);
		
//		PrinterAppl printerAppl = new PrinterAppl();
//		printerAppl.foo1();
//		
//		SomeClass.hello();
//		
//		System.out.println("Hello");
//		System.out.println("Hello");
//		
//		for (int i = 0; i < printers.length; i++) {
////			if (i == printers.length - 1)
////				printers[printers.length - 1].setReference(printers[0]);
////			else
//			printers[i].setReference(printers[(i + 1) % printers.length]);
//			// (0 + 1) % 4 == 1
//			// (1 + 1) % 4 == 2
//			// (2 + 1) % 4 == 3
//			// (3 + 1) % 4 == 0
//		}
		
		for (Printer printer : printers) {
			printer.start();
		}

//		for (int i = 0; i < printers.length; i++){
//			printers[i].start();
//		}
		
		printers[0].interrupt();
		System.out.println("");
		
		foo(50);
	}
	
	public static void foo(int sum) {
		int num;
		num = 10;
		System.out.println(sum);
		System.out.println(num);
	}
}
