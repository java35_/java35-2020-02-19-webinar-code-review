package telran.printer;

public class App {
	public static void main(String[] args) {
		Employee2 e = Employee2.builder()
						.id(4)
						.name("name")
						.surname("surname")
						.build();
		
		System.out.println(e);
	}
}
