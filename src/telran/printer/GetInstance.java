package telran.printer;

public class GetInstance {
	public static void main(String[] args) {
		
		// Constructor
		Employee e = new Employee(1, "name1", "surname1");

		
		// Pattern builder
		Employee e1 = Employee.builder()
				.id(e.getId())
				.name(e.getName())
				.surname(e.getSurname())
				.build();
		
		System.out.println(e1);
		
		// Static fabric method
//		LocalDateTime.from(temporal)
		
		// from �� ������� ����
		// of - �������� ���������
//		List.of("Hello", "Hi");
		
		// Patterm with
		Employee e2 = new Employee()
				.withId(4)
				.withName("name4")
				.withSurname("surname4");
		
		e2.withId(5);
		
	}
}
