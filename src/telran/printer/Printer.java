package telran.printer;

public class Printer extends Thread{

	private static int N_NUMBERS = 100;
	private static int N_PORTIONS = 10;
	private int number;
	private Printer reference;

	public Printer(int number, Printer reference) {
		this.number = number;
		this.reference = reference;
	}
	
	public Printer(int number){
		this(number, null);
	}
	
	public Printer getReference() {
		return reference;
	}

	public void setReference(Printer reference) {
		this.reference = reference;
	}

	@Override
	public void run(){
		try {
			sleep(Long.MAX_VALUE);
		} catch (InterruptedException e){
			for (int i = 0; i < (N_NUMBERS / N_PORTIONS); i++){
				for (int j = 0; j < N_PORTIONS; j++){
					System.out.print("" + number + " ");
				}
				System.out.println();
				reference.interrupt();
				try {
					sleep(Long.MAX_VALUE);
				} catch (InterruptedException e1) {
					System.out.print("");
					if ((i + 1) >= (N_NUMBERS / N_PORTIONS))
						reference.interrupt();
				}
			}	
		}
	}
}
