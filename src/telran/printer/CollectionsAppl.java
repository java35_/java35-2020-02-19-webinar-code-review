package telran.printer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectionsAppl {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		
		list.add("One");
		list.add("Two");
		list.add("Two");
		list.add("Three");
		list.add("Four");
		
		// classic for
		// ��������� ���������, �� i �� ��� ����-�� ����������
		// � ��� �����, ���� ����� �������� ���� �������
//		for (int i = 0; i < list.size(); i++) {
//			System.out.println(list.get(i));
//		}
		
		// for (forEach style)
		// ��������� ���������, �� i �� �� ����������
		// � ��� �����, ���� ����� �������� ���� �������
//		for (String string : list) {
//			System.out.println(string);
//		}
		
		// Iterator
		// ����� ����� ��������� ��������� �� ������-�� ��������
//		Iterator<String> itr = list.iterator();
//		while (itr.hasNext()) {
//			if (itr.next().equals("Two"))
//				itr.remove();
//		}

		// forEach
		list.forEach(el -> {
			if (el.contains("Two"))
				System.out.println(el);
		});
		
		// stream
	}
}
