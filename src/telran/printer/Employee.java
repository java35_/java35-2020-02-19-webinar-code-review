package telran.printer;

public class Employee {
	int id;
	String name;
	String surname;
	
	public Employee(int id, String name, String surname) {
		this.id = id;
		this.name = name;
		this.surname = surname;
	}
	
	public Employee() {
		
	}
	
	public Employee withId(int id) {
		this.id = id;
		return this;
	}
	
	public Employee withName(String name) {
		this.name = name;
		return this;
	}
	
	public Employee withSurname(String surname) {
		this.surname = surname;
		return this;
	}
	
	public static EmployeeBuilder builder() {
		return new EmployeeBuilder();
	}
	
	public static Employee from(Employee e) {
		return new Employee(e.id, e.name, e.surname);
	}
	
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", surname=" + surname + "]";
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public static class EmployeeBuilder {
		private int id;
		private String name;
		private String surname;
		
		public EmployeeBuilder id(int id) {
			this.id = id;
			return this;
		}

		public EmployeeBuilder name(String name) {
			this.name = name;
			return this;
		}

		public EmployeeBuilder surname(String surname) {
			this.surname = surname;
			return this;
		}
		
		public Employee build() {
			return new Employee(id, name, surname);
		}
	}
}